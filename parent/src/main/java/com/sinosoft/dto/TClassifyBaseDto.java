package com.sinosoft.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 10:11
 */
@Entity(name = "t_classify")
public class TClassifyBaseDto implements Serializable {
    @Id
    private String classcode;
    private String classname;
    private String classdesc;
    private String classlogo;
    private String operator;
    private Date inserttime;

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getClassdesc() {
        return classdesc;
    }

    public void setClassdesc(String classdesc) {
        this.classdesc = classdesc;
    }

    public String getClasslogo() {
        return classlogo;
    }

    public void setClasslogo(String classlogo) {
        this.classlogo = classlogo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public TClassifyBaseDto(String classcode, String classname, String classdesc, String classlogo, String operator, Date inserttime) {
        this.classcode = classcode;
        this.classname = classname;
        this.classdesc = classdesc;
        this.classlogo = classlogo;
        this.operator = operator;
        this.inserttime = inserttime;
    }

    public TClassifyBaseDto() {
    }
}
