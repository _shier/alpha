package com.sinosoft.dto;

import java.io.Serializable;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/17 16:42
 */
public class UserDto implements Serializable{
    private String username;
    private String password;
    private String usercode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public UserDto(String username, String password, String usercode) {
        this.username = username;
        this.password = password;
        this.usercode = usercode;
    }

    public UserDto() {
    }
}
