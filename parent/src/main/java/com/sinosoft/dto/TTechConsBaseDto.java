package com.sinosoft.dto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 15:08
 */
@Entity(name = "t_techcons")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TTechConsBaseDto implements Serializable {
    @Id
    private String concode;
    private String conname;
    private String condesc;
    private String conlogo;
    @ManyToOne(optional = true)
    @JoinColumn(name = "classcode")
    private TClassifyBaseDto classify;
    private String operator;
    private String inserttime;


    public String getConcode() {
        return concode;
    }

    public void setConcode(String concode) {
        this.concode = concode;
    }

    public String getConname() {
        return conname;
    }

    public void setConname(String conname) {
        this.conname = conname;
    }

    public String getCondesc() {
        return condesc;
    }

    public void setCondesc(String condesc) {
        this.condesc = condesc;
    }

    public String getConlogo() {
        return conlogo;
    }

    public void setConlogo(String conlogo) {
        this.conlogo = conlogo;
    }

    public TClassifyBaseDto getClassify() {
        return classify;
    }

    public void setClassify(TClassifyBaseDto classify) {
        this.classify = classify;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getInserttime() {
        return inserttime;
    }

    public void setInserttime(String inserttime) {
        this.inserttime = inserttime;
    }

    public TTechConsBaseDto(String concode, String conname, String condesc, String conlogo, TClassifyBaseDto classify, String operator, String inserttime) {
        this.concode = concode;
        this.conname = conname;
        this.condesc = condesc;
        this.conlogo = conlogo;
        this.classify = classify;
        this.operator = operator;
        this.inserttime = inserttime;
    }

    public TTechConsBaseDto() {
    }
}
