package com.cosmic.bms.servicebmsbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceBmsBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBmsBaseApplication.class, args);
	}
}
