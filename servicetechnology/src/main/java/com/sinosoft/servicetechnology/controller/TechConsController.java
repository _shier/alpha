package com.sinosoft.servicetechnology.controller;

import com.sinosoft.dto.TTechConsBaseDto;
import com.sinosoft.servicetechnology.services.TechConsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 15:13
 */
@RestController
public class TechConsController {
    private Logger logger= LoggerFactory.getLogger(TechConsController.class);
    @Autowired
    private TechConsService techConsService;
    @RequestMapping("/tech/findByConcode")
    public TTechConsBaseDto findByConcode(@RequestParam String concode){
        logger.info("/tech/findByConcode?concode="+concode);
        return techConsService.findByConcode(concode);
    }

    @RequestMapping("/tech/save")
    public TTechConsBaseDto save(@RequestBody TTechConsBaseDto dto){
        logger.info("/tech/save");
        return techConsService.save(dto);
    }
    @RequestMapping("/tech/deleteByConcode")
    public int deleteByConcode(@RequestParam String concode){
        logger.info("/tech/deleteByConcode");
        return techConsService.deleteByConcode(concode);
    }
    @RequestMapping("/tech/findAll")
    public List<TTechConsBaseDto> findAll(){
        return techConsService.findAll();
    }
}
