package com.sinosoft.servicetechnology.services;

import com.sinosoft.dto.TTechConsBaseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 15:13
 */
@Service
public interface TechConsService extends JpaRepository<TTechConsBaseDto,String> {
    TTechConsBaseDto findByConcode(String concode);
    int deleteByConcode(String concode);
    List<TTechConsBaseDto> findAll();
}
