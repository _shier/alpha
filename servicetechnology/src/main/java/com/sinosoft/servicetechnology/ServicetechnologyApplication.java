package com.sinosoft.servicetechnology;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EntityScan(basePackages={"com.sinosoft.dto"})
public class ServicetechnologyApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServicetechnologyApplication.class, args);
	}
}
