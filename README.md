# alpha

模块汇总，集大成者<br/>


- 本系统旨在学习springcloud微服务框架及其思想，以及前后端分离开发模式
- 后端使用springcloud框架
- 前端使用MVC框架Angular4渲染数据，使用Nodejs、Npm支持，使用Bootstrap做样式渲染<br/>
- 新增JPA依赖底层数据交互

2018-5-8
bms（企业管理软件 business management software）
- 新增 bms-服务-基础功能（bms-service-base）
- 新增 bms-服务-考勤模块（bms-service-attendance）
- 新增 bms-服务-实时聊天（bms-service-livechat）
- 新增 bms-服务-绩效考核（bms-service-performance）
- 新增 bms-服务-日程管理（bms-service-schedule）
- 新增 bms-服务-任务管理（bms-service-task）

2018-4-25
- 新增JPA依赖
- 确定项目核心功能

- 功能模块
1. 分类管理：增删改查
2. 技术控件管理：增删改查
3. 数据统计：分类统计/技术控件统计
4. 架构模拟：以鼠标拖动的形式，在web界面上模拟构建各种架构图；生成图片

![系统架构](https://gitee.com/uploads/images/2018/0423/102429_ca8a4dce_774287.png "QQ截图20180423102326.png")



