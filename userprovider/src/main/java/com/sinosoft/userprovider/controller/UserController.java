package com.sinosoft.userprovider.controller;

import com.sinosoft.userprovider.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sinosoft.dto.UserDto;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/17 16:33
 */
@RestController
public class UserController {
    private Logger logger= LoggerFactory.getLogger(UserController.class);
    @Autowired
    UserService userService;

    @RequestMapping("/user/getUserByCode")
    public UserDto getUserByCode(@RequestParam String usercode){
        logger.info("生产者 - controller - getUserByCode");
        UserDto dto =new UserDto();
        dto.setUsercode(usercode);
        return userService.getUser(dto);
    }

    @RequestMapping("/user/saveUser")
    public int saveUser(@RequestBody UserDto dto){
        logger.info("生产者 - controller - saveUser");
        return userService.saveUser(dto);
    }
    @RequestMapping("/user/updateUser")
    public int updateUser(@RequestBody UserDto dto){
        logger.info("生产者 - controller - updateUser");
        return userService.updateUser(dto);
    }
    @RequestMapping("/user/removeUser")
    public int removeUser(@RequestBody UserDto dto){
        logger.info("生产者 - controller - removeUser");
        return userService.removeUser(dto);
    }
    @RequestMapping("/user/listUser")
    public List<UserDto> listUser(@RequestBody UserDto dto){
        logger.info("生产者 - controller - listUser");
        return userService.listUser(dto);
    }
}
