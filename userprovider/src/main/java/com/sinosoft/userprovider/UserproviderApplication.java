package com.sinosoft.userprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@EnableEurekaClient
public class UserproviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserproviderApplication.class, args);
	}
}
