package com.sinosoft.userprovider.services;

import com.sinosoft.dto.UserDto;

import java.util.List;

public interface UserService {
    int saveUser(UserDto dto);
    int updateUser(UserDto dto);
    int removeUser(UserDto dto);
    UserDto getUser(UserDto dto);
    List<UserDto> listUser(UserDto dto);
}
