package com.sinosoft.serviceclassify.services;

import com.sinosoft.dto.TClassifyBaseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 10:20
 */
@Service
@Transactional
public interface ClassifyService extends JpaRepository<TClassifyBaseDto,String> {
    TClassifyBaseDto save(TClassifyBaseDto dto);
    int removeByClasscode(String classcode);
    TClassifyBaseDto findByClasscode(String classcode);
}
