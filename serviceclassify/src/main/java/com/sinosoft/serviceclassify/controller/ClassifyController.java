package com.sinosoft.serviceclassify.controller;

import com.sinosoft.dto.TClassifyBaseDto;
import com.sinosoft.serviceclassify.services.ClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 10:48
 */
@RestController
public class ClassifyController {
    @Autowired
    private ClassifyService classifyService;
    private Logger logger= LoggerFactory.getLogger(ClassifyController.class);
    @RequestMapping("/classify/getOne")
    public TClassifyBaseDto getOne(@RequestParam String classcode){
        return classifyService.findByClasscode(classcode);
    }
    @RequestMapping("/classify/save")
    public TClassifyBaseDto save(@RequestBody TClassifyBaseDto dto){
        return classifyService.save(dto);
    }
    @RequestMapping("/classify/removeByClasscode")
    public int removeByClasscode(@RequestParam String classcode){
        return classifyService.removeByClasscode(classcode);
    }
    @RequestMapping("/classify/findAll")
    public List<TClassifyBaseDto> findAll(){
        return classifyService.findAll(Sort.by("inserttime").descending());
    }
}