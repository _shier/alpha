package com.cosmic.bms.servicebmsattendance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceBmsAttendanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBmsAttendanceApplication.class, args);
	}
}
