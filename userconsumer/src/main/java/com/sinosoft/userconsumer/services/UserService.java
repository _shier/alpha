package com.sinosoft.userconsumer.services;

import com.sinosoft.dto.UserDto;

import java.util.List;

public interface UserService {
    UserDto getUserByCode(String usercode);
    int saveUser(UserDto dto);
    int removeUser(UserDto dto);
    int updateUser(UserDto dto);
    List<UserDto> listUsers(UserDto dto);

}
