package com.sinosoft.userconsumer.services.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sinosoft.dto.UserDto;
import com.sinosoft.userconsumer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/17 17:37
 */
@Service
public class UserServiceImpl implements UserService{
    private Logger logger= LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private RestTemplate restTemplate;
    @Override
    @HystrixCommand(fallbackMethod = "error")
    public UserDto getUserByCode(String usercode) {
        logger.info("消费者：consumer - getUserByCode.");
        return restTemplate.getForObject("http://userprovider/user/getUserByCode?usercode="+usercode,UserDto.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public int saveUser(UserDto dto) {
        logger.info("消费者：consumer - saveUser.");
        return restTemplate.getForObject("http://userprovider/user/saveUser?dto="+dto,Integer.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public int removeUser(UserDto dto) {
        logger.info("消费者：consumer - removeUser.");
        return restTemplate.getForObject("http://userprovider/user/removeUser?dto="+dto,Integer.class);
    }

    @Override
    public int updateUser(UserDto dto) {
        logger.info("消费者：");
        return 0;
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public List<UserDto> listUsers(UserDto dto) {
        logger.info("消费者：consumer - listUsers.");
        return restTemplate.getForObject("http://userprovider/user/listUsers?dto="+dto,List.class);
    }

    private String error(String para){
        return "error is found,"+para;
    }
}
