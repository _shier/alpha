package com.sinosoft.userconsumer.controller;

import com.sinosoft.dto.UserDto;
import com.sinosoft.userconsumer.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/17 17:33
 */
@RestController
public class UserController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    UserService userService;

    @RequestMapping("/user/getUserByCode")
    public UserDto getUserByCode(@RequestParam String usercode){
        logger.info("消费者：controller - getUserByCode" );
        return userService.getUserByCode(usercode);
    }
    @RequestMapping("/user/saveUser")
    public int saveUser(@RequestBody UserDto dto){
        logger.info("消费者：controller - saveUser" );
        return userService.saveUser(dto);
    }
    @RequestMapping("/user/removeUser")
    public int removeUser(@RequestBody UserDto dto){
        logger.info("消费者：controller - removeUser" );
        return userService.removeUser(dto);
    }
    @RequestMapping("/user/updateUser")
    public int updateUser(@RequestBody UserDto dto){
        logger.info("消费者：controller - updateUser" );
        return userService.updateUser(dto);
    }
    @RequestMapping("/user/listUsers")
    public List<UserDto> listUsers(@RequestBody UserDto dto){
        logger.info("消费者：controller - listUsers" );
        return userService.listUsers(dto);
    }
}
