package com.sinosoft.alphaapi.util;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/27 13:59
 */
public class RespEntity {
    private int code;
    private String msg;
    private Object data;

    public RespEntity(RespCode respCode) {
        this.code = respCode.getCode();
        this.msg = respCode.getMsg();
    }

    public RespEntity(RespCode respCode, Object data) {
        this(respCode);
        this.data = data;
    }
}