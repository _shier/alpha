package com.sinosoft.alphaapi.controller;

import com.sinosoft.alphaapi.services.impl.ClassifyServiceImpl;
import com.sinosoft.alphaapi.services.impl.TechConsServiceImpl;
import com.sinosoft.dto.TClassifyBaseDto;
import com.sinosoft.dto.TTechConsBaseDto;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 18:22
 */
@RestController
public class AlphaApiController {
    private Logger logger= LoggerFactory.getLogger(AlphaApiController.class);
    @Autowired
    private ClassifyServiceImpl classifyService;
    @Autowired
    private TechConsServiceImpl techConsService;

    @RequestMapping("/api/classify/save")
    public TClassifyBaseDto save(@RequestParam  String dtoJson){
        TClassifyBaseDto dto=new TClassifyBaseDto();

        return classifyService.save(dto);
    }
    @RequestMapping(value = "/api/classify/removeByClasscode",produces = {"application/json;charset=UTF-8" })
    public int removeByClasscode(@RequestParam String classcode){
        return classifyService.removeByClasscode(classcode);
    }
    @RequestMapping("/api/classify/findByClasscode")
    public TClassifyBaseDto findByClasscode(@RequestParam String classcode){
        return classifyService.findByClasscode(classcode);
    }
    @RequestMapping("/api/tech/findByConcode")
    public TTechConsBaseDto findByConcode(@RequestParam String concode){
        return techConsService.findByConcode(concode);
    }
    @RequestMapping("/api/tech/deleteByConcode")
    public int deleteByConcode(@RequestParam String concode){
        return techConsService.deleteByConcode(concode);
    }
    @RequestMapping(value = "/api/tech/findAll",produces = {"application/json;charset=UTF-8" })
    public List<TTechConsBaseDto> findAll(HttpResponse response){
        logger.info("======/api/tech/findAll");
        return techConsService.findAll();
    }
}
