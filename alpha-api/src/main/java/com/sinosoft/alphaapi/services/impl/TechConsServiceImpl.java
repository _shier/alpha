package com.sinosoft.alphaapi.services.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sinosoft.alphaapi.services.TechConsService;
import com.sinosoft.dto.TTechConsBaseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/27 9:10
 */
@Service
public class TechConsServiceImpl implements TechConsService {
    private Logger logger= LoggerFactory.getLogger(TechConsServiceImpl.class);
    @Autowired
    private RestTemplate restTemplate;
    @Override
    @HystrixCommand(fallbackMethod = "error")
    public TTechConsBaseDto findByConcode(String concode) {
        return restTemplate.getForObject("http://service-technology/tech/findByConcode?concode="+concode,TTechConsBaseDto.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public int deleteByConcode(String concode) {
        return restTemplate.getForObject("http://service-technology/tech/deleteByConcode?concode="+concode,Integer.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public List<TTechConsBaseDto> findAll() {
        logger.info("====findAll====");
        return restTemplate.getForObject("http://service-technology/tech/findAll",List.class);
    }

    @Value("errorMsg")
    private String error;
    private String error(String para){
        return error+" : "+para;
    }
}
