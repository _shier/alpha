package com.sinosoft.alphaapi.services.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sinosoft.alphaapi.services.ClassifyService;
import com.sinosoft.dto.TClassifyBaseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/27 9:10
 */
@Service
public class ClassifyServiceImpl implements ClassifyService {
    private Logger logger= LoggerFactory.getLogger(ClassifyServiceImpl.class);
    @Autowired
    private RestTemplate restTemplate;
    @Override
    @HystrixCommand(fallbackMethod = "error")
    public TClassifyBaseDto save(TClassifyBaseDto dto) {
        return restTemplate.getForObject("http://service-classify/classify/save?dto="+dto,TClassifyBaseDto.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public int removeByClasscode(String classcode) {
        return restTemplate.getForObject("http://service-classify/classify/removeByClasscode?classcode="+classcode,Integer.class);
    }

    @Override
    @HystrixCommand(fallbackMethod = "error")
    public TClassifyBaseDto findByClasscode(String classcode) {
        return restTemplate.getForObject("http://service-classify/classify/findByClasscode?classcode="+classcode,TClassifyBaseDto.class);
    }
    private String error(String para){
        return "error is found,"+para;
    }
}
