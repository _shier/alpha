package com.sinosoft.alphaapi.services;

import com.sinosoft.dto.TTechConsBaseDto;

import java.util.List;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 15:13
 */
public interface TechConsService{
    TTechConsBaseDto findByConcode(String concode);
    int deleteByConcode(String concode);
    List<TTechConsBaseDto> findAll();
}
