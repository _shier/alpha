package com.sinosoft.alphaapi.services;

import com.sinosoft.dto.TClassifyBaseDto;
import org.springframework.stereotype.Service;

/**
 * @author Wenzheng.Ma
 * @desc
 * @date 2018/04/25 10:20
 */
public interface ClassifyService {
    TClassifyBaseDto save(TClassifyBaseDto dto);
    int removeByClasscode(String classcode);
    TClassifyBaseDto findByClasscode(String classcode);
}
